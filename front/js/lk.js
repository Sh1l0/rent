document.querySelector('.bike__list').addEventListener('click', event => {
  if(event.target.classList.contains('button')) {
    let id = event.target.getAttribute('data-order-id');
    console.log(id);
    if(id) {
  //  xhr.onreadystatechange = () => {
    //  if(xhr.readyState === 4) {
  //
  //    }
//    }
//    console.log(`/api/order/${id}`);
//    xhr.open('DELETE', `/api/order/${id}`);
//    xhr.setRequestHeader('Content-Type', 'application/json');
//    xhr.send();
    fetch(`/api/order/${id}`, {
    method: "DELETE",
    credentials: "include",
    headers: {
      "Content-Type":"application/json"
    }
  }
  ).then(() => {
    event.target.parentNode.parentNode.remove();
    if(!document.querySelector('.bike__list').contains(document.querySelector('.bike__rented'))) {
      const div = document.createElement('div');
      div.innerHTML = "Нет арендованных велосипедов";
      div.className = "no-data";
      document.querySelector('.bike__list').appendChild(div);
    }
  });
 }
}
});

'use strict';

function init() {
  let page = 1;
  loadCatalog(page++);
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  document.querySelector('#loadMore')
    .addEventListener("click", () => {
        loadCatalog(page++);
      });
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
}

function loadCatalog(page) {
  disableButtonLoadMore();
  const list = getBikes(getPointId(), page);
  console.log(list)
  list.then(value => {
    value.json().then(data => {
      enableButtonLoadMore();
      //console.log(value.text());
      appendCatalog(data.bikesList);
      showButtonLoadMore(data.hasMore);
    });

  });
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
}

function getBikes(pointId, pageNumber) {
  if(!pointId) {
    pointId = '';
  }
  return fetch(`/api/catalog/${pointId}?page=${pageNumber}`, {
    headers: {'Accept': 'application/json, text/plain, */*'}
  });

}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  const bikeList = document.querySelector('#bikeList');

  for (let item of items) {
    console.log(item.img);
    const div = document.createElement('div');
    div.className = "bikeList bike";
    const name = document.createElement('div');
    name.className = "bike__name";
    name.innerHTML = item.name;
    const cost = document.createElement('div');
    cost.className = "bike__cost";
    cost.innerHTML = `Стоимость за минуту - ${item.cost}₽`;
    const imgWrapper = document.createElement('div');
    imgWrapper.className = "bike__img-wrapper"
    const img = document.createElement('img');
    img.className = "bike__img";
    img.src = "/images/" + item.img;
    img.width = "248";
    const button = document.createElement('a');
    button.className = "button";
    button.innerHTML = "Aрендовать";
    button.href = `/order/${item._id}`;
    if(item.isRented) {
      button.className += " disabled";
    }
    imgWrapper.appendChild(img);
    console.log(item);
    div.appendChild(imgWrapper);
    div.appendChild(name);
    div.appendChild(cost);
    div.appendChild(button);
    console.log(div);
    bikeList.appendChild(div);
  }
}

function showButtonLoadMore(hasMore) {
  if(hasMore) {
    document.querySelector('#loadMore').classList.remove("hidden");
  }
  else {
    document.querySelector('#loadMore').classList.add("hidden");
  }
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
}

function disableButtonLoadMore() {
  document.querySelector('#loadMore > button').disabled = true;
  document.querySelector('#loadMore > button').classList.add('disabled')
  // заблокируй кнопку "загрузить еще"
}

function enableButtonLoadMore() {
  document.querySelector('#loadMore > button').disabled = false;
  document.querySelector('#loadMore > button').classList.remove('disabled')
  // разблокируй кнопку "загрузить еще"
}

function getPointId() {
  const url = document.URL;
  let id = url.split("catalog").pop();
  console.log(id);
  return id.split('/')[1];
  // сделай определение id выбранного пункта проката
}


document.addEventListener('DOMContentLoaded', init)

const errors = [
  "Поле не должно быть пустым",
  "В поле должны содержаться только цифры",
  "Номер карты должен состоять из 16 цифр",
  "CVV код должен состоять только из трёх цифр",
  "Дата введена не правильно"
]

let isFormValid = true;

function trimAll (str) {
  return str.replace(/\s/g, "");
}

function cvvValidate (cvv) {
  return /^\d{3}$/.test(cvv);
}

function cardNumberMask (cardNumberValue) {
  let maskedValue = "";
  for(let i = 0, j = 0; i < cardNumberValue.length; i++, j++) {
    if(j === 4) {
      maskedValue += " ";
      j = 0;
    }
    maskedValue += cardNumberValue[i];
  }
  return maskedValue;

}

function cardNumberValidate (cardNumberValue) {
  return /^\d{16}$/.test(cardNumberValue);
}

function getCardDate (cardDateValue) {
  return {
    month: Number(cardDateValue.substr(0, 2)),
    year: Number(cardDateValue.substr(2, 2))
  };
}

function cardDateValidation (date) {
  const currentYear = new Date().getFullYear() % 100;
  if(
       date.year - currentYear > 5
    || date.year - currentYear < 0
    || date.month > 12
    || date.month < 1
  ) {
    return false;
  }
  return true;

}

function getUrl(params) {
  let qParams = params.split('&');
  let obj = {};
  for (let par of qParams) {
    let tmp = par.split('=');
    obj[`${tmp[0]}`] = tmp[1];
  }
  if(obj.back_url) {
    obj['back_url'] = `/order/${obj['back_url'].split('%').pop().slice(2)}`;
  }
  return obj;
}

function cardDateMask(date) {
  return `${date.month}/${date.year}`;
}

function renderError (error, elem) {
  const errBlock = document.createElement('div');
  errBlock.className = "pointer";
  errBlock.innerHTML = errors[error];
  elem.classList.add('form__input_red');
  if(elem.nextElementSibling) {
    elem.parentNode.replaceChild(errBlock, elem.nextElementSibling)
    return;
  }
  elem.parentNode.appendChild(errBlock)
}

function hideError (elem) {
  if(elem.nextElementSibling){
    elem.nextElementSibling.remove();
    elem.classList.remove('form__input_red');
  }
}

function isFieldEmpty (elem) {
  return trimAll(elem.value) === "";
}

function ifAllNumbers (elem) {
  return !/\D/.test(elem.value);
}




document.getElementById('cardNumber').addEventListener('blur', event => {
  event.target.value = trimAll(event.target.value);
  let errorIndex = null;
  switch (true) {
    case !ifAllNumbers(event.target) && !isFieldEmpty(event.target): {
        errorIndex = 1;
        break;
    }
    case !cardNumberValidate(event.target.value) && !isFieldEmpty(event.target): {
      errorIndex = 2;
      break;
    }
  }
  if(errorIndex) {
    isFormValid = false;
    renderError(errorIndex, event.target);
    return;
  }
  isFormValid = true;
  hideError(event.target);
  event.target.value = cardNumberMask(event.target.value);
});


document.getElementById('cardDate').addEventListener('blur', event => {
  event.target.value = event.target.value.replace(/\D+/g, "");
  let errorIndex = null;
  switch (true) {
    case !cardDateValidation(getCardDate(event.target.value)) && !isFieldEmpty(event.target): {
      errorIndex = 4;
      break;
    }
    case !isFieldEmpty(event.target): {
      event.target.value = cardDateMask(getCardDate(event.target.value));
      break;
    }
  }
  if (errorIndex) {
    isFormValid = false;
    renderError(errorIndex, event.target);
    return;
  }
  isFormValid = true;
  hideError(event.target);
});


document.getElementById('cvv').addEventListener('blur', event => {
  event.target.value = trimAll(event.target.value);
  let errorIndex = null;
  switch(true) {
    case !ifAllNumbers(event.target) && !isFieldEmpty(event.target): {
      errorIndex = 1;
      break;
    }
    case !cvvValidate(event.target.value) && !isFieldEmpty(event.target): {
      errorIndex = 3;
      break;
    }
  }
  if(errorIndex) {
    isFormValid = false;
    renderError(errorIndex, event.target);
    return;
  }
  isFormValid = true;
  hideError(event.target);
});


document.querySelector('.submit').addEventListener('click', event => {
  event.preventDefault();
  const inputs = document.querySelectorAll('.form__input');
  for(let input of inputs) {
    if(isFieldEmpty(input)) {
      isFormValid = false;
      renderError(0, input);
    }
  }
  if(isFormValid) {
    const body = {
      number: `${document.querySelector('#cardNumber').value}`,
      date: `${document.querySelector('#cardDate').value}`,
      cvv: `${document.querySelector('#cvv').value}`
    }
    console.log(body);
    const req = fetch('/api/card-requisites', {
      method: "PUT",
      headers: {
        "Content-Type":"application/json"
      },
      body: JSON.stringify(body),
      credentials: "include"
    });
    req.then(res => {
      if(!res.ok) {
        throw new Error(`Код ошибки ${res.status}`);
        return;
      }
      const url = document.URL.split('?');
        href = getUrl(url.pop()).back_url;
        if(href) {
          window.location.href = href;
        }
        else {
          window.location.href = '/lk';
        }

    }).catch(error => {
      console.log(error);
    });
  }
});

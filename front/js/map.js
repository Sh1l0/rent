ymaps.ready(init);

   function init(){
       const rentMap = new ymaps.Map("map", {
           center: [55.02, 82.92],
           zoom: 12
       });
       const pointers = api.getPointers();
       pointers.then(arr => {
         for(let point of arr) {
           let obj = new ymaps.Placemark([point.coordinates[0], point.coordinates[1]], {
           hintContent: 'Точка выдачи',
           balloonContentBody: `<div class="map__point">
                                  <div class="map__title">Пункт по адресу ${point.address}</div>
                                  <br />
                                  <a href='/catalog/${point._id}' class='button map__button'>Выбрать велосипед</a>
                                </div>`,
         });
        rentMap.geoObjects.add(obj);
         }
       });

   }
